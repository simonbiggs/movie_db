# Movie Database

## Introduction
This project aims to store the movies you have locally downloaded, and their metadata. A web interface is provided which allows you to open the containing folder (windows only currently), filter them(WIP), and view all their metadata(WIP). This also serves as a playground to learn new technologies, currently Rust and Vue.js. 

## Building
`cargo build`

## Running
`cargo run`

You will also require an API key for [OMDb API](https://www.omdbapi.com/) and a YAML configuration file in the top level of this repo. The YAML should look something like below:

```
api_key: your_api_key
movies_dir: C:\Full\Path\To\Movies
bind_port: 8080
```
This has only been tested on windows, you may encounter strange behaviour on other platforms.
## TODO
* [ ] Add a dirty flag, loading the add movie page will then refresh directories
* [x] Refactor the methods to be individual routes rather than a big function
  * [ ] Look into further breaking them out
* [ ] Remove stateful API
  * [x] Implement add movie
  * [ ] Update JS to use add movie
  * [ ] Delete confirm api
* [ ] Add custom movie insertion in JS
* [ ] Refactor to allow multiple threads (requires non-stateful API)
* [ ] Implement filtering
* [ ] Implement details screen on front end
* [ ] Add a native frontend
  * [ ] Consider Azul vs Druid