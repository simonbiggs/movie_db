extern crate actix_web;
extern crate regex;
use actix_web::{HttpRequest, HttpResponse, web, Result};
use actix_files::NamedFile;
use regex::Regex;
use super::omdb_wrapper;
use super::dir_handler;
use super::db_handler;
use super::movie_types;
use std::cell::Cell;

pub struct ServerState {
    db:Cell<Option<db_handler::DB>>,
    nice:omdb_wrapper::Nicify,
    conf:movie_types::Config,
    movie_parser:Regex,
    curr_entry:Cell<Option<omdb_wrapper::Reply>>,
    curr_folder:Cell<String>,
    join_char:String,
}

pub fn movies(data: web::Data<ServerState>) -> HttpResponse {
    let temp_db = data.db.take().unwrap();
    let body = serde_json::to_string(temp_db.get_movies()).unwrap();
    data.db.replace(Some(temp_db));
    HttpResponse::Ok()
        .content_type("application/json; charset=utf-8")
        .body(body)
}

pub fn folders(data: web::Data<ServerState>) -> HttpResponse {
    let temp_db = data.db.take().unwrap();
    let movies = temp_db.get_movies();
    let body = serde_json::to_string(
                &data.nice.nicify(
                    dir_handler::enumerate_folders(
                        &data.conf.movies_dir,
                        movies
                    )
                )
            ).unwrap();
    data.db.replace(Some(temp_db));
    HttpResponse::Ok()
        .content_type("application/json; charset=utf-8")
        .body(body)
}

pub fn defns() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("application/json; charset=utf-8")
        .body(movie_types::defn_options())
}

pub fn add(data: web::Data<ServerState>, entry: web::Json<movie_types::EntryFromClient>) -> HttpResponse {
    match movie_types::defn_from_int(entry.definition) {
        Some(defn) => {
            let mut temp_db = data.db.take().unwrap();
            let to_add = movie_types::Entry {
                name: entry.name.to_owned(),
                watched: entry.watched,
                definition: defn,
                released: entry.released.to_owned(),
                imdb: entry.imdb,
                imdbid: entry.imdbid.to_owned(),
                metacritic: entry.metacritic,
                rtomatoes: entry.rtomatoes,
                length: entry.length,
                director: entry.director.to_owned(),
                genre: entry.genre.to_owned(),
                plot: entry.plot.to_owned(),
                rating: entry.rating,
                comments: entry.comments.to_owned(),
                folder: entry.folder.to_owned(),
                poster: entry.poster.to_owned()
            };
            match temp_db.add_movie(to_add) {
                Err(e) => HttpResponse::BadRequest().body(format!("Failed to add movie {:?}", e)),
                Ok(_)  => HttpResponse::Ok().body("")
            }
        },
        None => HttpResponse::BadRequest().body("Invalid definitition")
    }
}

// TODO, consider breaking this up into other functions further
pub fn handle_api_req(data: web::Data<ServerState>, path: web::Path<String>) -> HttpResponse {
    // &* magic converts String to &str (*String = str)
    match data.movie_parser.captures(&*path.into_inner()) {
        Some(caps) => {
            match &caps["api"] {
                "movie" => {
                    match omdb_wrapper::search_movie(&caps[2], &data.conf.api_key, false) {
                        Ok(mut entry) => {
                            let folder = caps[3].to_string().replace("%20", " ");
                            entry.defn = Some(dir_handler::find_defn(&data.conf.movies_dir, &folder));
                            let body = serde_json::to_string(&entry).unwrap();
                            data.curr_entry.set(Some(entry));
                            data.curr_folder.set(folder);
                            HttpResponse::Ok()
                                .content_type("application/json; charset=utf-8")
                                .body(&body)
                        },
                        Err(_) => {
                            HttpResponse::Ok()
                                .content_type("application/json; charset=utf-8")
                                .body("{\"found\": false}")
                        }
                    }
                },
                "tv" => {
                    match omdb_wrapper::search_movie(&caps[2], &data.conf.api_key, true) {
                        Ok(mut entry) => {
                            let folder = caps[3].to_string().replace("%20", " ");
                            entry.defn = Some(dir_handler::find_defn(&data.conf.movies_dir, &folder));
                            let body = serde_json::to_string(&entry).unwrap();
                            data.curr_entry.set(Some(entry));
                            data.curr_folder.set(folder);
                            HttpResponse::Ok()
                                .content_type("application/json; charset=utf-8")
                                .body(&body)
                        },
                        Err(_) => {
                            HttpResponse::Ok()
                                .content_type("application/json; charset=utf-8")
                                .body("{\"found\": false}")
                        }
                    }
                },
                "confirm" => {
                    // /api/confirm&defn=\d&watched=\d
                    // To be removed once the js side is updated
                    let reply = data.curr_entry.take();
                    if reply.is_none() {
                        return HttpResponse::BadRequest().body("You need to make a movie call first")
                    }
                    let defn = movie_types::defn_from_int(caps[2].parse::<i64>().unwrap_or(-1));
                    if defn.is_none() { return HttpResponse::BadRequest().body("Bad definition") }
                    let watched = caps[3].parse::<i64>().unwrap_or(-1);
                    if watched != 0 && watched != 1 { return HttpResponse::BadRequest().body("Bad watched") }

                    let to_commit = data.make_entry(reply.unwrap(), defn.unwrap(), watched);
                    let mut temp_db = data.db.take().unwrap();
                    let result_addrow = temp_db.add_movie(to_commit);
                    data.db.replace(Some(temp_db));
                    match result_addrow {
                        Ok(_) => HttpResponse::Ok().body(""),
                        Err(e) => HttpResponse::BadRequest().body(format!("Failed to commit: {:?}", e))
                    }
                },
                _ => HttpResponse::NotFound().body("This api does not exist"),
            }
        },
        None => HttpResponse::NotFound().body("This api does not exist"),
    }
}

pub fn open_dir(data: web::Data<ServerState>, path: web::Path<String>) -> HttpResponse {
    let real_folder = path.replace("%20", " ");
    let full_path = [data.conf.movies_dir.to_owned(), data.join_char.to_owned(), real_folder].concat();
    if dir_handler::open_folder(&full_path) {
        HttpResponse::Ok().body("")
    } else {
        HttpResponse::NotFound().body("Not implemented")
    }
}

impl ServerState {
    pub fn new(
        db:db_handler::DB,
        nice:omdb_wrapper::Nicify,
        conf:movie_types::Config
    ) -> ServerState {
        let join_char = if conf.movies_dir.ends_with("\\") {
            String::new()
        } else {
            String::from("\\")
        };
        ServerState {
            db:Cell::new(Some(db)),
            nice,
            conf,
            movie_parser:Regex::new(r"(?P<api>[^&]*)&[^=]*=([^&]*)&[^=]*=(.*)").unwrap(),
            curr_entry:Cell::new(None),
            curr_folder:Cell::new(String::new()),
            join_char
        }
    }

    pub fn make_entry(&self, reply:omdb_wrapper::Reply, defn:movie_types::Defn, watched:i64) -> movie_types::Entry {
        let ratings = omdb_wrapper::ratings_parse(&reply.ratings);
        movie_types::Entry {
            name: String::from(reply.title.as_str()),
            watched: watched,
            definition: defn,
            released: String::from(reply.released.as_str()),
            imdb: ratings[0],
            imdbid: String::from(reply.imdb_id.as_str()),
            metacritic: ratings[2],
            rtomatoes: ratings[1],
            length: omdb_wrapper::run_parse(&reply.runtime),
            director: String::from(reply.director.as_str()),
            genre: String::from(reply.genre.as_str()),
            plot: String::from(reply.plot.as_str()),
            rating: -1,
            comments: String::new(),
            folder: self.curr_folder.take(),
            poster: String::from(reply.poster.as_str()),
        }
    }
}

pub fn home(_req: HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("web/home_page.html")?)
}

pub fn css(_req: HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("web/styles.css")?)
}

pub fn home_js(_req: HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("web/home_page.js")?)
}

pub fn add_movie(_req: HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("web/add_movie.html")?)
}

pub fn add_js(_req: HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("web/add_movie.js")?)
}

pub fn about(_req: HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("web/about_page.html")?)
}

pub fn success(_req: HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("web/success.svg")?)
}

pub fn failure(_req: HttpRequest) -> Result<NamedFile> {
    Ok(NamedFile::open("web/failure.svg")?)
}