#[macro_use]
extern crate serde_derive;
extern crate serde_yaml;
extern crate serde_json;
extern crate actix_web;

use std::fs;

mod db_handler;
mod movie_types;
mod dir_handler;
mod omdb_wrapper;
mod my_server;
use actix_web::{App, HttpServer, web};

fn main() {
    // Load configuration
    let conf_str = fs::read_to_string("movie_db.yml").expect("Can't read configuration");
    let conf:movie_types::Config = serde_yaml::from_str(&conf_str).expect("Invalid YAML");
    println!("{:?}", conf);

    let bind_str = format!("127.0.0.1:{}", conf.bind_port);
    println!("http://{}", bind_str);

    HttpServer::new(|| {
        App::new()
            .data(
                my_server::ServerState::new(
                    db_handler::DB::new().unwrap(),
                    omdb_wrapper::Nicify::new(),
                    serde_yaml::from_str(
                        &fs::read_to_string("movie_db.yml").expect("Can't read configuration")
                    ).expect("Invalid YAML")
                )
            )
            .route("/api/movies", web::get().to(my_server::movies))
            .route("/api/folders", web::get().to(my_server::folders))
            .route("/api/defns", web::get().to(my_server::defns))
            .route("/api/add", web::post().to(my_server::add))
            .route("/api/{path}", web::get().to(my_server::handle_api_req))
            .route("/", web::get().to(my_server::home))
            .route("/home_js", web::get().to(my_server::home_js))
            .route("/css", web::get().to(my_server::css))
            .route("/add", web::get().to(my_server::add_movie))
            .route("/add_js", web::get().to(my_server::add_js))
            .route("/about", web::get().to(my_server::about))
            .route("/success.svg", web::get().to(my_server::success))
            .route("/failure.svg", web::get().to(my_server::failure))
            .route("/open/{path}", web::get().to(my_server::open_dir))
    })
    .workers(1)
    .bind(bind_str)
    .expect(&format!("Can not bind to port {}", conf.bind_port))
    .run()
    .expect("Failed to run the server");
}
