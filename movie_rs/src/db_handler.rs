extern crate rusqlite;

use rusqlite::{Connection, Result};
use std::path::Path;
use super::movie_types;
use std::vec::Vec;
use rusqlite::NO_PARAMS;

const SCHEMA:&str = "CREATE TABLE if not exists movies (
                    name            TEXT NOT NULL,
                    watched         INTEGER,
                    definition      INTEGER,
                    released        TEXT NOT NULL,
                    imdb            INTEGER,
                    imdbid          TEXT NOT NULL,
                    metacritic      INTEGER,
                    rtomatoes       INTEGER,
                    length          INTEGER,
                    director        TEXT NOT NULL,
                    genre           TEXT NOT NULL,
                    plot            TEXT NOT NULL,
                    rating          INTEGER,
                    comments        TEXT NOT NULL,
                    folder          TEXT NOT NULL,
                    poster          TEXT
                )";


pub struct DB {
    cache: Vec<movie_types::Entry>,
    conn: Option<Connection>,
}

impl DB {
    fn open_db() -> Result<Connection> {
        let conn = Connection::open(Path::new("movies.db"))?;
        conn.execute(SCHEMA, NO_PARAMS)?;
        Ok(conn)
    }

    fn load_movies(&mut self) -> Result<()> {
        if self.conn.is_none() {
            self.conn = Some(DB::open_db()?);
        }
        let conn = self.conn.as_ref().unwrap();
        let mut stmt = conn.prepare("SELECT name, watched, definition, released, imdb, imdbid, metacritic, rtomatoes, length, director, genre, plot, rating, comments, folder, poster FROM movies")?;
        let mut rows = stmt.query(NO_PARAMS)?;
        while let Some(row) = rows.next()? {
            self.cache.push(movie_types::Entry{
                name: row.get(0)?,
                watched: row.get(1).unwrap(),
                definition: movie_types::defn_from_int(row.get(2).unwrap()).unwrap(),
                released: row.get(3).unwrap(),
                imdb: row.get(4).unwrap(),
                imdbid: row.get(5).unwrap(),
                metacritic: row.get(6).unwrap(),
                rtomatoes: row.get(7).unwrap(),
                length: row.get(8).unwrap(),
                director: row.get(9).unwrap(),
                genre: row.get(10).unwrap(),
                plot: row.get(11).unwrap(),
                rating: row.get(12).unwrap(),
                comments: row.get(13).unwrap(),
                folder: row.get(14).unwrap(),
                poster: row.get(15).unwrap(),
            });
        }
        Ok(())
    }

    pub fn new() -> Result<DB> {
        let mut db = DB {
            cache: Vec::new(),
            conn:  Some(DB::open_db()?),
        };
        match db.load_movies() {
            Ok(_) => {db.conn = None; Ok(db)},
            Err(x) => Err(x),
        }
    }

    pub fn get_movies(&self) -> &Vec<movie_types::Entry> {
        &self.cache
    }

    pub fn add_movie(&mut self, entry:movie_types::Entry) -> Result<()> {
        if self.conn.is_none() {
            self.conn = Some(DB::open_db()?);
        }
        let conn = self.conn.as_ref().unwrap();
        let query = format!("INSERT INTO movies VALUES (\"{}\", {}, {}, \"{}\", {}, \"{}\", {}, {}, \"{}\", \"{}\", \"{}\", \"{}\", {}, \"{}\", \"{}\", \"{}\")",
                                    entry.name,
                                    entry.watched,
                                    movie_types::int_from_defn(&entry.definition),
                                    entry.released,
                                    entry.imdb,
                                    entry.imdbid,
                                    entry.metacritic,
                                    entry.rtomatoes,
                                    entry.length,
                                    entry.director,
                                    entry.genre,
                                    entry.plot.replace("\"", "\"\""),
                                    entry.rating,
                                    entry.comments,
                                    entry.folder.replace("%20", " "),
                                    entry.poster);
        // println!("{}", &query);
        conn.execute(&query, NO_PARAMS)?;
        self.cache.push(entry);
        Ok(())
    }
}

impl Clone for DB {
    fn clone(&self) -> Self {
        DB {
            cache: self.cache.clone(),
            conn: None
        }
    }
}