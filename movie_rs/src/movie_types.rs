#[derive(Debug, Clone, Serialize)]
pub enum Defn {
    Unknown,
    Potato,
    P480,
    P720,
    P1080
}

#[derive(Debug, Clone, Serialize)]
pub struct Entry {
    pub name:            String,
    pub watched:         i64,
    pub definition:      Defn,
    pub released:        String,
    pub imdb:            i64,
    pub imdbid:          String,
    pub metacritic:      i64,
    pub rtomatoes:       i64,
    pub length:          i64,
    pub director:        String,
    pub genre:           String,
    pub plot:            String,
    pub rating:          i64,
    pub comments:        String,
    pub folder:          String,
    pub poster:          String,
}

#[derive(Deserialize)]
pub struct EntryFromClient {
    pub name:            String,
    pub watched:         i64,
    pub definition:      i64,
    pub released:        String,
    pub imdb:            i64,
    pub imdbid:          String,
    pub metacritic:      i64,
    pub rtomatoes:       i64,
    pub length:          i64,
    pub director:        String,
    pub genre:           String,
    pub plot:            String,
    pub rating:          i64,
    pub comments:        String,
    pub folder:          String,
    pub poster:          String,
}

pub fn defn_from_int(d:i64) -> Option<Defn> {
    match d {
        0 => Some(Defn::Unknown),
        1 => Some(Defn::Potato),
        2 => Some(Defn::P480),
        3 => Some(Defn::P720),
        4 => Some(Defn::P1080),
        _ => None,
    }
}

pub fn int_from_defn(d:&Defn) -> i64 {
    match d {
        Defn::Unknown   => 0,
        Defn::Potato    => 1,
        Defn::P480      => 2,
        Defn::P720      => 3,
        Defn::P1080     => 4,
    }
}

pub fn defn_options() -> String {
    String::from("[\"Unknown\", \"Potato\", \"480p\", \"720p\", \"1080p\"]")
}

#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    pub api_key: String,
    pub movies_dir: String,
    pub bind_port: u32,
}