extern crate reqwest;
extern crate regex;

use std::vec::Vec;
use std::collections::HashMap;
use regex::Regex;

#[derive(Deserialize, Debug, Serialize)]
pub struct Reply {
    #[serde(rename = "Title")]
    pub title:    String,
    #[serde(rename = "Released")]
    pub released: String,
    #[serde(rename = "Runtime")]
    pub runtime:  String,
    #[serde(rename = "Genre")]
    pub genre:    String,
    #[serde(rename = "Director")]
    pub director: String,
    #[serde(rename = "Plot")]
    pub plot:     String,
    #[serde(rename = "Poster")]
    pub poster:   String,
    #[serde(rename = "Ratings")]
    pub ratings:  Vec<HashMap<String,String>>,
    #[serde(rename = "imdbID")]
    pub imdb_id:   String,
    pub defn:      Option<i64>,
}

pub fn search_movie(title:&str, apikey:&str, tv:bool) -> Result<Reply, serde_json::Error> {
    let mut request = format!("http://www.omdbapi.com/?apikey={}&t={}&plot=full", apikey, title.replace(" ", "+"));
    if tv {
        request += "&type=series"
    }
    serde_json::from_str(
        &reqwest::get(&request).unwrap().text().unwrap()
    )
}

#[derive(Clone)]
pub struct Nicify {
    remove_date: Regex,
    remove_def: Regex,
    remove_squares: Regex,
    remove_date2: Regex,
    remove_trailing_space: Regex,
}

impl Nicify {
    pub fn new() -> Nicify {
        Nicify {
            remove_date: Regex::new(r"( \.)?\(\d{4}\)").unwrap(),
            remove_def: Regex::new(r"( \.)?\d{3,4}p").unwrap(),
            remove_squares: Regex::new(r"( \.)?\[[^]]*\]").unwrap(),
            remove_date2: Regex::new(r"( \.)?(19|20)\d\d").unwrap(),
            remove_trailing_space: Regex::new(r" *$").unwrap(),
        }
    }

    fn make_nice(&self, input:&str) -> String {
        let a = self.remove_date.replace_all(input, "");
        let b = self.remove_def.replace_all(&a, "");
        let c = self.remove_squares.replace_all(&b, "");
        let d = self.remove_date2.replace_all(&c, "");
        self.remove_trailing_space.replace_all(&d, "").to_string()
    }

    pub fn nicify(&self, input:Vec<String>) -> Vec<(String, String)> {
        input.into_iter().map(
            |x| (self.make_nice(&x), x)
        ).collect()
    }
}

pub fn ratings_parse(input:&Vec<HashMap<String,String>>) -> Vec<i64> {
    let mut ratings = vec![-1,-1,-1];
    for r in input {
        match r["Source"].as_ref() {
            "Internet Movie Database" => {ratings[0] = imdb_parse(&r["Value"]);},
            "Rotten Tomatoes" => {ratings[1] = rt_parse(&r["Value"]);},
            "Metacritic" => {ratings[2] = meta_parse(&r["Value"]);},
            _ => (),
        }
    }
    ratings
}

// Todo avoid compiling regex each time
pub fn imdb_parse(imdb:&str) -> i64 {
    // 7.2/10
    let re = Regex::new(r"(\d+\.?\d?)/10").unwrap();
    let fl = re.captures(imdb).unwrap()[1].parse::<f64>().unwrap();
    fl.round() as i64
}

fn rt_parse(rt:&str) -> i64 {
    let re = Regex::new(r"(\d+)%").unwrap();
    re.captures(rt).unwrap()[1].parse::<i64>().unwrap()
}

fn meta_parse(meta:&str) -> i64 {
    let re = Regex::new(r"(\d+)/100").unwrap();
    re.captures(meta).unwrap()[1].parse::<i64>().unwrap()
}

pub fn run_parse(run:&str) -> i64 {
    let re = Regex::new(r"(\d+) min").unwrap();
    re.captures(run).unwrap()[1].parse::<i64>().unwrap()
}