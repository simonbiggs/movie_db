use std::vec::Vec;
use std::fs;
use std::collections::HashSet;
use super::movie_types;
use std::path::Path;
use std::process::Command;
// TODO: https://docs.rs/notify/4.0.10/notify/
// https://github.com/passcod/notify/tree/v4.0.10#notify
pub fn enumerate_folders(root:&str, movies:&Vec<movie_types::Entry>) -> Vec<String> {
    let folders:Vec<String> = fs::read_dir(root).unwrap()
                                                .into_iter()
                                                .map(|x| String::from(x.unwrap().file_name().to_str().unwrap()))
                                                .filter(|x| (x != "$RECYCLE.BIN") && (x != "System Volume Information")) // Remove garbage dirs
                                                .collect();
    // folders.remove(0); // Remove recycle bin
    let mut seen = HashSet::new();
    for m in movies {
        seen.insert(m.folder.clone());
    }
    folders.into_iter().filter(|f| !seen.contains(f) ).collect()
}

pub fn find_defn(root:&str, folder:&str) -> i64 {
    let full_path:String;
    if root.ends_with(r"\") {
        full_path = [root, folder].join("");
    } else {
        full_path = [root, folder].join(r"\");
    }

    for item in fs::read_dir(full_path).unwrap() {
        let file_name = String::from(item.unwrap().file_name().to_str().unwrap());
        if file_name.contains("1080") {
            return movie_types::int_from_defn(&movie_types::Defn::P1080)
        } else if file_name.contains("720") {
            return movie_types::int_from_defn(&movie_types::Defn::P720)
        } else if file_name.contains("480") {
            return movie_types::int_from_defn(&movie_types::Defn::P480)
        } else if file_name.contains("360") || file_name.contains("240") {
            return movie_types::int_from_defn(&movie_types::Defn::Potato)
        }
    }

    movie_types::int_from_defn(&movie_types::Defn::Unknown)
}

pub fn open_folder(path:&str) -> bool {
    if !Path::new(path).exists() {
        return false
    }
    match Command::new("explorer").arg(path).status() {
        Err(_x) => false,
        Ok(_x) => true,
    }
}