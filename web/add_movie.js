// Refactor to make use of Vue more
//      v-on:click for buttons
//      v-model for inputs

var folders = new Vue({
    el: '#folders',
    data: {
        folders: []
    }
})

var defns = new Vue({
    el: '#modal-options',
    data: {
        definitions: []
    }
})

var cust_defns = new Vue({
    el: '#DefinitionCustom',
    data: {
        definitions: []
    }
})

var modal = document.getElementById("addModal");
var modalContent = document.getElementById("modalContent");
var modalHeader = document.getElementById("modal-title");
var definition = document.getElementById("Definition");
var watched = document.getElementById("watched");
var notification = document.getElementById("notification");
var notification_success = document.getElementById("notification_success");
var notification_failure = document.getElementById("notification_failure");
var failure_text = document.getElementById("failure-text");
var custom = document.getElementById("custom-modal");
var custTitle = document.getElementById("Title")
var custDefinition = document.getElementById("Definition")
var custWatched = document.getElementById("Watched")
var custPlot = document.getElementById("Plot")
var custPoster = document.getElementById("Poster")
var custRuntime = document.getElementById("Runtime")
var custReleased = document.getElementById("Released")
var custGenre = document.getElementById("Genre")
var custDirector = document.getElementById("Director")
var custimdbID = document.getElementById("imdbID")
var custidmd = document.getElementById("idmd")
var custRottenTomatoes = document.getElementById("Rotten Tomatoes")
var custMetacritic = document.getElementById("Metacritic")

function hideCustom() {
    custom.style.display = "none";
}

// Get the close element
var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
    modal.style.display = "none";
}
document.getElementsByClassName("close")[1].onclick = hideCustom;
document.getElementById("no-custom").onclick = hideCustom;
document.getElementById("no").onclick = function () {showCustom();modal.style.display = "none";}
document.getElementById("confirm").onclick = function () {
    confirm_req = new XMLHttpRequest();
    confirm_req.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                // TODO: Pull notifications into their own fucntion
                // TODO: Update movie list on success
                notification_success.style.display = "block";
                notification_failure.style.display = "none";
                modal.style.display = "none";
                notification.style.display = "block";
                setTimeout(function () {
                    notification.style.display = "none";
                    xmlhttp.open("GET", "/api/folders", true);
                    xmlhttp.send(null);
                }, 1400);
            } else {
                failure_text.firstChild.data = "Failed to add movie"
                notification_failure.style.display = "block";
                notification_success.style.display = "none";
                modal.style.display = "none";
                notification.style.display = "block";
                setTimeout(function () {notification.style.display = "none"}, 1400);
                console.log(confirm_req.responseText);
            }
        }
    }
    confirm_req_url = "/api/confirm&defn=" + definition.value + "&watched=" + watched.value;
    console.log(confirm_req_url);
    confirm_req.open("GET", confirm_req_url, true);
    confirm_req.send(null);
};

function showCustom() {
    custom.style.display = "block";
}

function add(event) {
    // alert("You clicked on " + event.target.id);
    // xmlhttp.onreadystatechange = function() {console.log("I got called wtf")}
    var addhttp = new XMLHttpRequest();
    var idx = Number(event.target.id);
    var title = event.target.parentElement.childNodes[1].value;
    var req_url = "/api/movie&title=" + title + "&folder=" + folders.folders[idx][1];
    modalHeader.firstChild.data = "Confirm Movie - " + title
    // console.log(req_url);
    addhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(addhttp.responseText);
            var resp = JSON.parse(addhttp.responseText);
            if (resp.Title == undefined) {
                failure_text.firstChild.data = "Failed to find movie"
                notification_failure.style.display = "block";
                notification_success.style.display = "none";
                notification.style.display = "block";
                setTimeout(function () {notification.style.display = "none"; showCustom();}, 1400);
            }
            else {
                modalContent.firstChild.data = resp.Plot;
                definition.value = resp.defn;
                watched.value = 1;
                modal.style.display = "block";
            }
        }
    }
    addhttp.open("GET", req_url, true);
    addhttp.send(null);
}

function add_handlers() {
    for (i = 0; i < folders.folders.length; i++) {
        document.getElementById(i).addEventListener("click", add, false);
    }
    if (folders.folders.length == 0) {
        document.getElementById("nothing").style.display = "block";
    } else {
        document.getElementById("nothing").style.display = "none";
    }
}

var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        folders.folders = JSON.parse(xmlhttp.responseText);
    }
    setTimeout(add_handlers, 50); // Need to wait for Vue
}
xmlhttp.open("GET", "/api/folders", true);
xmlhttp.send(null);

var defnhttp = new XMLHttpRequest();
defnhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        defns.definitions = JSON.parse(defnhttp.responseText);
        cust_defns.definitions = defns.definitions;
    }
}
defnhttp.open("GET", "/api/defns", true);
defnhttp.send();