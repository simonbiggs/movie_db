var movies = new Vue({
    el: '#movies',
    data: {
        movies: []
    }
})

function openDir(event) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "/open/" + event.target.id, true);
    xmlhttp.send(null);
}

function addHandles() {
    buttons = document.getElementsByClassName("open-dir-button")
    for (i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener("click", openDir, false);
    };
}

var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        movies.movies = JSON.parse(xmlhttp.responseText);
    }
    if (movies.movies.length == 0) {
        document.getElementById("nothing").style.display = "block";
    } else {
        document.getElementById("nothing").style.display = "none";
        setTimeout(addHandles, 50);
    }
}
xmlhttp.open("GET", "/api/movies", true);
xmlhttp.send( null );
